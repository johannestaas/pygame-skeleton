import pygame
from pygame.locals import *

screen = pygame.display.set_mode((800,600))
pygame.display.set_caption('Pygame Tutorial')
back = pygame.Surface(screen.get_size())
back.fill((255,255,255))

war = pygame.image.load('images/warrior.gif').convert()

print dir(war)
pos = (400,300)

running = True
while(running):
    for event in pygame.event.get():
        if event.type != KEYDOWN and event.type != KEYUP:
            continue
        if event.key == K_ESCAPE:
            running = False
        x, y = pos
        d = 10
        if event.key == K_w:
            y = y - d
        if event.key == K_a:
            x = x - d
        if event.key == K_s:
            y = y + d
        if event.key == K_d:
            x = x + d
        pos = (x, y)
        back.fill((255,255,255))
        back.blit(war, pos)
    screen.blit(back, (0,0))
    pygame.display.flip()
